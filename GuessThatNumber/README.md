﻿# GuessThatNumber Game

Guess at a randomly generated number, while receiving insightful (yet discouraging) input from your console.

## Installation

1. Clone the Week2 repository on to your desktop: `git clone https://github.com/mitchellpash/SeedPaths-Week2.git SeedPaths-Week2`
2. Open the `SeedPaths-Week2/GuessThatNumber/GuessThatNumber.sln` solution file
3. Press Ctrl+F5 to run!

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

[latest](https://github.com/mitchellpash/SeedPaths-Week2/commit/fed50a67d0a696dba98c5ee74b49fea9e03c99e6)
- Optimized with Resharper

## Credits

[int.Parse StackOverflow](http://stackoverflow.com/questions/894263/how-to-identify-if-a-string-is-a-number)

## License

Copyright (c)2015 Mitchell Pash <mitch@mitchpash.com> [@mitchellpash](https://www.twitter.com/mitchellpash)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
