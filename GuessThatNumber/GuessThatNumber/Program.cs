﻿using System;

namespace GuessThatNumber
{
    internal static class Program
    {
        //Make a new random number to guess, we are not getting more than one random number to test so we directly make a 'new' Random that gives a number between 1 and 100. Remeber that .Next() is not inclusive.
        private static int _numberToGuess = new Random().Next(1, 101);

        private static void Main(string[] args)
        {
            //track number of guesses
            var numberOfGuesses = 0;
            //set keep guessing option (false when user guesses the number properlly)
            var keepGuessing = true;

            //while the user has not guessed the random number
            while (keepGuessing)
            {
                //Get user input
                Console.Write("Pick a number between 1 and 100: ");
                var usersNumber = Console.ReadLine();

                //Use a validator to make sure the user's input is a number between 1 and 100, continue until the input is valid
                if (usersNumber == null || !ValidateInput(usersNumber)) continue;

                numberOfGuesses++; // increment the numberOfGuesses
                var parsedInput = int.Parse(usersNumber); //Make our user's input a number we can work with

                //Check if the user's number is 'The One'
                if (parsedInput == _numberToGuess)
                {
                    Console.WriteLine("That's it! Eureka!");
                    Console.WriteLine("It took you {0} guesses", numberOfGuesses);
                    keepGuessing = false;
                }
                else
                {
                    //track how much further the user needs to go
                    var howFarOff = 0;
                    

                    //check to see if the user's guess is too high
                    if (IsGuessTooHigh(parsedInput)) 
                    {
                        //we will give the user feedback based on howFarOff the user is from the _numberToGuess
                        howFarOff = parsedInput - _numberToGuess;
                        if (howFarOff >= 50)
                        {
                            Console.WriteLine("That guess is just too damn high...\n");
                        }
                        else if (howFarOff >= 20)
                        {
                            Console.WriteLine("Eh you're kind of close... lower man...\n");
                        }
                        else if (howFarOff >= 10)
                        {
                            Console.WriteLine("You're just a little to high...\n");
                        }
                        else
                        {
                            Console.WriteLine("A little lower and you're there!\n");
                        }
                    }
                    //check to see if the user's guess is too low
                    else if (IsGuessTooLow(parsedInput))
                    {
                        //we will give the user feedback based the _numberToGuess from howFarOff the user is
                        howFarOff = _numberToGuess - parsedInput;
                        if (howFarOff >= 50)
                        {
                            Console.WriteLine("That guess is just too damn low...\n");
                        }
                        else if (howFarOff >= 20)
                        {
                            Console.WriteLine("Eh you're kind of close... higher man...\n");
                        }
                        else if (howFarOff >= 10)
                        {
                            Console.WriteLine("You're just a little to low...\n");
                        }
                        else
                        {
                            Console.WriteLine("A little higher and you're there!\n");
                        }
                    }
                }
            }

            //End of Program
            Console.ReadKey();
        }

        public static bool ValidateInput(string userInput)
        {
            //check to make sure the user's input is a number
            int n;
            if (!int.TryParse(userInput, out n))
                Console.WriteLine("That isn't even a number... come on\n");
            else
            {
                //check to make sure that the users input is a valid number between 1 and 100.
                if (n > 0 && n < 101)
                    return true;
                else
                    Console.WriteLine("What part about pick a number between 1 and 100 wasn't clear enough?\n");
            }

            return false;
        }

        public static void SetNumberToGuess(int number)
        {
            //Override global variable holding the number the user needs to guess.
            _numberToGuess = number;
        }

        public static bool IsGuessTooHigh(int userGuess)
        {
            //return true if the number guessed by the user is too high
            return userGuess > _numberToGuess;
        }

        public static bool IsGuessTooLow(int userGuess)
        {
            //return true if the number guessed by the user is too low
            return userGuess < _numberToGuess;
        }
    }
}