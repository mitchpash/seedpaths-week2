﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceThrower1000
{
    class Program
    {
        static Random random = new Random();
        static StringBuilder stringBuilder = new StringBuilder();

        static void Main(string[] args)
        {
            //Grab input from User
            Console.Write("Dice Notation: ");
            string diceNotaion = Console.ReadLine();

            ThrowDice(diceNotaion);

            //End of Program
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
        /// <summary>
        /// Takes a string in Dice Notation and prints the dice each time it is flipped.
        /// </summary>
        /// <param name="diceNotaion"></param>
        private static void ThrowDice(string diceNotaion)
        {

            List<int> averagesList = new List<int>();
            List<string> diceList = diceNotaion.Split(' ').ToList();

            foreach (var dice in diceList)
            {
                //Make sure our list is clear when we begin
                stringBuilder.Clear();

                //Grab the index of char 'd'
                int indexOfD = dice.IndexOf('d');

                //Grab the numberOfFlips (left of char d's index)
                for (int i = 0; i < indexOfD; i++)
                {
                    stringBuilder.Append(dice[i]);
                }
                //Input: 1234d56 Output: 1234

                //Convert to int
                int numberOfFlips = int.Parse(stringBuilder.ToString());

                //Clear stringBuilder
                stringBuilder.Clear();

                //Grab the numberOfSides (right of char d's index)
                for (int i = indexOfD + 1; i < dice.Length; i++)
                {
                    stringBuilder.Append(dice[i]);
                }
                //Input: 1234d56 Output: 56

                //Convert to int
                int numberOfSides = int.Parse(stringBuilder.ToString());

                Console.WriteLine("Rolling a {0} sided dice {1} times: ", numberOfSides, numberOfFlips);
                for (int i = 0; i < numberOfFlips; i++)
                {
                    var next = random.Next(numberOfSides);
                    averagesList.Add(next);
                    Console.Write("| {0} ", next);
                }
                Console.Write("|\n");
                Console.WriteLine("# The average roll was: {0}", averagesList.Average());
                //whitespace
                Console.WriteLine();
            }
            
        }
    }
}
