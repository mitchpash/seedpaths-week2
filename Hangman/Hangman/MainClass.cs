﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Hangman
{
    internal class MainClass
    {
        private static readonly List<string> WordBank = new List<string>() { "PONIES", "EMU", "WINDOW", "LILLY", "GERMS", "GROSS" };

        private static void Main(string[] args)
        {
            // Get rid of the scroll bars by making the buffer the same size as the window
            Console.Clear();
            Console.SetWindowSize(70, 35);
            Console.BufferWidth = 70;
            Console.BufferHeight = 35;

            HangmanGame hangmanGame = new HangmanGame(WordBank);
            Console.ReadKey();
        }
    }

    internal class HangmanGame
    {
        private string _playerName = "";
        private string _wordToGuess = "";
        private readonly Random _rng = new Random();
        private int _lives = 15;
        private List<char> _guesses = new List<char>();
        /// <summary>
        /// Begin a new game of Hangman with a wordBank
        /// </summary>
        /// <param name="wordBank"></param>
        public HangmanGame(IReadOnlyList<string> wordBank)
        {
            //Pick a word from the WordBank
            _wordToGuess = wordBank[_rng.Next(wordBank.Count)];

            WelcomeUser();

            //While the user has not guessed the word
            while (GetFilledWord().Contains('_') && _lives > 0)
            {
                Console.Clear();
                DrawGui();
                Console.Write("Pick a letter or word: ");
                var userInput = Console.ReadLine().ToUpper();
                //Check for valid userInput (expected to be alpha-non-numeric)
                if (IsValidInput(userInput))
                {
                    //Detect indivdual words and letters alike
                    foreach (var letter in userInput)
                    {
                        Console.Clear();
                        DrawGui();
                        //Check for userInput in _wordToGuess
                        if (_wordToGuess.Contains(letter.ToString()))
                        {
                            if (!_guesses.Contains(letter))
                            {
                                //Not guessed yet - Add to list
                                _guesses.Add(letter);
                                DrawResponce(SuccessfulGuessResponce(), ConsoleColor.Green);
                            }
                            else
                            {
                                //Allready Guessed
                                DrawResponce(AlreadyGuessedResponce());
                            }
                        }
                        else if (!_guesses.Contains(letter))
                        {
                            //Add guess to list
                            _guesses.Add(letter);
                            _lives--;
                            if (_lives < 1)
                            {
                                break;
                            }
                            //Letter guessed was not in _wordToGuess
                            DrawResponce(IncorrectGuessResponce(), ConsoleColor.Red);
                        }
                        else
                        {
                            //Allready Guessed
                            DrawResponce(AlreadyGuessedResponce());
                        }
                    }
                }
                else
                {
                    //Tell the user their input was not valid
                    DrawResponce(InvalidResponce(), ConsoleColor.DarkMagenta);
                }
            }
            //If there are no more black spaces returned by GetFilledWord at the end of the game then congradulate the user
            if (!GetFilledWord().Contains('_'))
            {
                Console.Clear();
                Console.WriteLine("You're right! The word was {0}!", _wordToGuess);
                Console.WriteLine("Congrats! You saved yourself from being hung!");
                Thread.Sleep(1500);
                DrawResponce(SuccessResponce(), refeshRate: 50, messageColor: ConsoleColor.Green);
            }
            //If there are '_' spaces returned by GetFilledWord at the end of the game then inform them they failed
            else
            {
                Console.Clear();
                Console.WriteLine("Oh no! The word was actually {0}...", _wordToGuess);
                Console.WriteLine("You failed to guess the word in time... OFF TO THE NOOSE!");
                Thread.Sleep(1500);
                DrawResponce(DeathResponce(), refeshRate: 50, messageColor: ConsoleColor.DarkRed);
            }
        }

        /// <summary>
        /// Returns a string with filled in correct guesses and '_' for all not guessed letters
        /// </summary>
        /// <returns></returns>
        private string GetFilledWord()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var c in _wordToGuess)
            {
                //Add a '_' for every not guessed letter
                stringBuilder.Append(_guesses.Contains(c) ? c : '_');
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Asks the user for their name and welcome's them to the game while also providing a quick tutoiral on the game
        /// </summary>
        private void WelcomeUser()
        {
            //Ask user for their name and welcome them
            Console.Write("What is your name?: ");
            _playerName = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Hello, {0}", _playerName);
            Thread.Sleep(1000);
            //Respond with welcomeResponce - slowed down
            DrawResponce(WelcomeResponce(), refeshRate: 20, toastTimer: 2500);
        }

        /// <summary>
        /// Draw and present relevant game data to the console above the user input
        /// </summary>
        private void DrawGui()
        {
            //Display current player
            Console.WriteLine("#Player: {0}", _playerName);
            //Display number of guesses left
            Console.WriteLine("#Lives: {0}", _lives);
            //Display currently guessed letters
            Console.Write("#Already Guessed Letters: ");
            foreach (var guess in _guesses)
            {
                Console.Write("{0} ", guess);
            }
            Console.WriteLine();
            //Get a formatted string revaling the correct guesses
            foreach (var c in GetFilledWord())
            {
                Console.Write("{0} ", c);
            }
            Console.WriteLine();
            //Display visual representation of hangman
        }

        /// <summary>
        /// Checks input for alpha-non-numeric values with Regex
        /// </summary>
        /// <param name="userInput"></param>
        /// <returns></returns>
        private bool IsValidInput(string userInput)
        {
            var r = new Regex("^[a-zA-Z]*$");
            return r.IsMatch(userInput);
        }

        /// <summary>
        /// Takes in a message and displays it in the console as a 'Responce' handler
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageColor"></param>
        /// <param name="toastTimer"></param>
        /// <param name="refeshRate"></param>
        private static void DrawResponce(IEnumerable<string> message, ConsoleColor messageColor = ConsoleColor.Yellow, int toastTimer = 700, int refeshRate = 0)
        {
            //Set console color to custom color if defined, otherwise use _DefaultConsoleColor
            Console.ForegroundColor = messageColor;
            //Print each line in message in relation to it's refreshRate
            foreach (var line in message)
            {
                Console.WriteLine(line);
                Thread.Sleep(refeshRate);
            }
            //Set console color back to default
            Console.ResetColor();
            //Keep message around during toastTimer
            Thread.Sleep(toastTimer);
        }

        /// <summary>
        /// Returns "Welcome to Hangman"
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<string> WelcomeResponce()
        {
            return new[]
            {
                @" __          __  _                            _         ",
                @" \ \        / / | |                          | |        ",
                @"  \ \  /\  / /__| | ___ ___  _ __ ___   ___  | |_ ___   ",
                @"   \ \/  \/ / _ \ |/ __/ _ \| '_ ` _ \ / _ \ | __/ _ \  ",
                @"   _\  /\  /  __/ | (_| (_) | | | | | |  __/ | || (_) | ",
                @"  | |\/| \/ \___|_|\___\___/|_| |_| |_|\___|  \__\___/  ",
                @"  | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __        ",
                @"  |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \       ",
                @"  | |  | | (_| | | | | (_| | | | | | | (_| | | | |      ",
                @"  |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|      ",
                @"                       __/ |                            ",
                @"                      |___/                             "
            };
        }

        /// <summary>
        /// Returns "Invalid Input" to a Responce handler
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<string> InvalidResponce()
        {
            return new[]
            {
                @"┬┌┐┌┬  ┬┌─┐┬  ┬┌┬┐  ┬┌┐┌┌─┐┬ ┬┌┬┐",
                @"││││└┐┌┘├─┤│  │ ││  ││││├─┘│ │ │ ",
                @"┴┘└┘ └┘ ┴ ┴┴─┘┴─┴┘  ┴┘└┘┴  └─┘ ┴ "
            };
        }

        /// <summary>
        /// Returns "You Already Guessed That" to a Responce handler
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<string> AlreadyGuessedResponce()
        {
            return new[]
            {
                @"┬ ┬┌─┐┬ ┬  ┌─┐┬  ┬─┐┌─┐┌─┐┌┬┐┬ ┬  ┌─┐┬ ┬┌─┐┌─┐┌─┐┌─┐┌┬┐  ┌┬┐┬ ┬┌─┐┌┬┐",
                @"└┬┘│ ││ │  ├─┤│  ├┬┘├┤ ├─┤ ││└┬┘  │ ┬│ │├┤ └─┐└─┐├┤  ││   │ ├─┤├─┤ │ ",
                @" ┴ └─┘└─┘  ┴ ┴┴─┘┴└─└─┘┴ ┴─┴┘ ┴   └─┘└─┘└─┘└─┘└─┘└─┘─┴┘   ┴ ┴ ┴┴ ┴ ┴ "
            };
        }

        /// <summary>
        /// Returns "You Guessed Correctly" to a Responce handler
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<string> SuccessfulGuessResponce()
        {
            return new[]
            {
                @"┬ ┬┌─┐┬ ┬  ┌─┐┬ ┬┌─┐┌─┐┌─┐┌─┐┌┬┐  ┌─┐┌─┐┬─┐┬─┐┌─┐┌─┐┌┬┐┬  ┬ ┬┬",
                @"└┬┘│ ││ │  │ ┬│ │├┤ └─┐└─┐├┤  ││  │  │ │├┬┘├┬┘├┤ │   │ │  └┬┘│",
                @" ┴ └─┘└─┘  └─┘└─┘└─┘└─┘└─┘└─┘─┴┘  └─┘└─┘┴└─┴└─└─┘└─┘ ┴ ┴─┘ ┴ o"
            };
        }

        /// <summary>
        /// Returns "That Guess Was Incorrect" to a Responce handler
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<string> IncorrectGuessResponce()
        {
            return new[]
            {
                @"┌┬┐┬ ┬┌─┐┌┬┐  ┌─┐┬ ┬┌─┐┌─┐┌─┐  ┬ ┬┌─┐┌─┐  ┬┌┐┌┌─┐┌─┐┬─┐┬─┐┌─┐┌─┐┌┬┐",
                @" │ ├─┤├─┤ │   │ ┬│ │├┤ └─┐└─┐  │││├─┤└─┐  │││││  │ │├┬┘├┬┘├┤ │   │ ",
                @" ┴ ┴ ┴┴ ┴ ┴   └─┘└─┘└─┘└─┘└─┘  └┴┘┴ ┴└─┘  ┴┘└┘└─┘└─┘┴└─┴└─└─┘└─┘ ┴ "
            };
        }

        private static IEnumerable<string> DeathResponce()
        {
            return new[]
            {
                @"               ...",
                @"             ;::::;",
                @"           ;::::; :;",
                @"         ;:::::'   :;",
                @"        ;:::::;     ;.",
                @"       ,:::::'       ;           OOO\",
                @"       ::::::;       ;          OOOOO\",
                @"       ;:::::;       ;         OOOOOOOO",
                @"      ,;::::::;     ;'         / OOOOOOO",
                @"    ;:::::::::`. ,,,;.        /  / DOOOOOO",
                @"  .';:::::::::::::::::;,     /  /     DOOOO",
                @" ,::::::;::::::;;;;::::;,   /  /        DOOO",
                @";`::::::`'::::::;;;::::: ,#/  /          DOOO",
                @":`:::::::`;::::::;;::: ;::#  /            DOOO",
                @"::`:::::::`;:::::::: ;::::# /              DOO",
                @"`:`:::::::`;:::::: ;::::::#/               DOO",
                @" :::`:::::::`;; ;:::::::::##                OO",
                @" ::::`:::::::`;::::::::;:::#                OO",
                @" `:::::`::::::::::::;'`:;::#                O",
                @"  `:::::`::::::::;' /  / `:#",
                @"   ::::::`:::::;'  /  /   `#",
            };
        }

        private static IEnumerable<string> SuccessResponce()
        {
            return new[]
            {
                @"                            __",
                @"                           /  \",
                @"                          |    |",
                @"            _             |    |",
                @"          /' |            | _  |",
                @"         |   |            |    |",
                @"         | _ |            |    |",
                @"         |   |            |    |",
                @"         |   |        __  | _  |",
                @"         | _ |  __   /  \ |    |",
                @"         |   | /  \ |    ||    |",
                @"         |   ||    ||    ||    |       _---.",
                @"         |   ||    ||    |. __ |     ./     |",
                @"         | _. | -- || -- |    `|    /      //",
                @"         |'   |    ||    |     |   /`     (/",
                @"         |    |    ||    |     | ./       /",
                @"         |    |.--.||.--.|  __ |/       .|",
                @"         |  __|    ||    |-'            /",
                @"         |-'   \__/  \__/             .|",
                @"         |       _.-'                 /",
                @"         |   _.-'      /             |",
                @"         |            /             /",
                @"         |           |             /",
                @"         `           |            /",
                @"          \          |          /'",
                @"           |          `        /",
                @"            \                .'",
                @"            |                |",
                @"            |                |",
                @"            |                |",
                @"            |                |"
            };
        } 
    }
}